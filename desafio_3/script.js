let dados = null;
const usuarios = document.getElementsByClassName('usuarios')[0];

let requisicao = new XMLHttpRequest();
requisicao.open('GET', 'https://api-stage.imobisales.com.br/captei/rest/teste');
requisicao.send(null);

requisicao.onreadystatechange = () => {
    if (requisicao.readyState === 4) {
        dados = JSON.parse(requisicao.responseText);
        console.log(dados);

        if (dados !== null) {
            dados.forEach((valor, indice) => {
                let linha = `
                    <div>
                        <div>${indice + 1}</div>
                        <div class="nome">${valor.name}</div>
                        <div class="cpf">${valor.cpf}</div>
                    </div>`;
                usuarios.innerHTML += linha; 
            });
        }
    }
}

